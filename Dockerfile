FROM python:3.7

WORKDIR /app

RUN pip3 install requests
RUN pip3 install flask

COPY HoeWarmlsHetlnDelft.py /app
CMD ["python3", "./HoeWarmlsHetlnDelft.py"]