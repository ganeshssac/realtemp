import requests, json 
  

test="http://weerlive.nl/api/json-data-10min.php?key=42873aa681&locatie=delft&temp"

# Enter your API key here 
api_key = "42873aa681"

  
# base_url variable to store url 
base_url = "http://weerlive.nl/api/json-data-10min.php?"
  
# Give city name 
city_name = input("Enter city name : ") 
  
# complete_url variable to store 
# complete url address 
complete_url = base_url + "key=" + api_key + "&locatie=" + city_name + "&temp"

# print(complete_url)
  
# get method of requests module 
# return response object 
response = requests.get(complete_url) 
  
# json method of response object  
# convert json format data into 
# python format data 
x = response.json() 

# print(x)
  
# Now x contains list of nested dictionaries 
# Check the value of "cod" key is equal to 
# "404", means city is found otherwise, 
# city is not found 
result = x['liveweer']
if result !=404:
    for data in result:
        # print(data['plaats'], data['temp'])



        
    
        current_temperature = data['temp']
        temperature = int(float(current_temperature))
        current_city= data['plaats']
    
        
    
    
        # print following values 
        print("The Current Temperature in " + current_city + " city is " + str(temperature) + " degree celcius")
        
  
else: 
    print(" City Not Found ") 